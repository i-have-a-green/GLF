��    #      4      L      L     M     c     i     z     �  "   �     �  !   �     �     �       
   #     .  H   J  V   �     �     �  	          "        8  9   ?     y     �  
   �  !   �     �     �     �  
             !     /     F  �  W     �                    $     ,     A     R     h     q     v     �     �  I   �  8        A     T  	   W     a     d     |  $   �     �     �     �     �     �     	     "	     3	     ;	     ?	     N	  	   _	   * Champs obligatoires Autre Choisir un sujet Choisir une civilité Civilité : Devenir ambassadeur / ambassadrice Devenir partenaire Découvrir toutes nos actualités Email :* Envoyer Faire un don ou suivre un don Formulaire Histoire mission et valeurs J'ai pris connaissance et accepte les <a href="%s">mentions légales</a> Je reconnais avoir pris connaissance et accepte les <a href="%s">mentions légales</a> Je souhaite... * Madame Message : Monsieur M’inscrire à un évènement GLF Nom :* Obtenir des informations concernant les activités de GLF Organisme : Proposer une collaboration Prénom :* Rejoindre le réseau des artistes Rejoindre l’équipe Renseignez votre email Retour aux actualités S'inscrire Tous Téléphone : Voir tous les artistes Être bénévole Project-Id-Version: ihag
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2022-08-16 08:09+0000
PO-Revision-Date: 2022-09-12 14:10+0000
Last-Translator: 
Language-Team: English (UK)
Language: en_GB
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.6.2; wp-6.0.1
X-Domain: ihag *Mandatory fields Other Pick a subject Pick a title Title : Become an ambassador Become a sponsor Discover all our news Email :* Send Donate or check in a donation Contact form History, missions and values I acknowledge having read and accepted the <a href="%s">legal notices</a> I acknowledge having read and accepted the legal notices I would like to :* Ms Message : Mr Sign up for a GLF event Last name :* Get information about GLF activities Organization : Suggest a cooperation First name :* Join the artists network Join the team Fill in your email adress Back to the news Sign up All Phone number : See more artists Volunteer 