let modaleZoom = document.getElementsByClassName('zoom-image')

Array.prototype.forEach.call(modaleZoom, function (el) {
    el.addEventListener('click',function (e) {

        e.stopPropagation()

        let fullImage = el.dataset.zoom


        let modaleContainer = document.createElement("div")
        modaleContainer.classList.add('modaleContainer')

        let modale = document.createElement("div")
        modale.innerHTML = fullImage
        modale.classList.add('modale')

        let modaleClose = document.createElement("button")
        modaleClose.classList.add('modaleCloseBtn')
        modaleClose.innerText = '✗'

        modaleContainer.appendChild(modale)
        modaleContainer.appendChild(modaleClose)
        document.body.appendChild(modaleContainer)

        setTimeout(function () {
            modaleContainer.style.opacity = '1';
        },100)

        // Gestion de la fermeture du modale
        document.addEventListener('click', function(e) {
            if ( !modale.contains(e.target) ) {
                modaleContainer.style.opacity = '0';
                setTimeout(function () {
                    modaleContainer.remove();
                },400)
            }
        })
    })
})