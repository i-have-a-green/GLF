
// Contact
if(document.forms.namedItem("contact-form")){
    let form = document.forms.namedItem("contact-form");
    form.addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        fetch(resturl + 'action-rest', {
            method: 'POST',
            body: new FormData(form),
            headers: {'X-WP-Nonce': wpApiSettings.nonce},
            cache: 'no-cache',
        })
        .then(
            function(response) {
                if (response.status !== 200) { console.log(response.status); return;}
                response.json().then(function(data) {
                    document.getElementById('submitContact').style.display ='none';
                    document.getElementById('responseContact').innerHTML = 'Votre message vient d\'être envoyé !';
                    if (document.getElementsByTagName("html")[0].lang == "en-GB") {
                        document.getElementById('responseContact').innerHTML = 'Your message has been sent !';
                    }
                });
            }
        )
        .catch(function(err) {
            console.log('Fetch Error :-S', err);
        });
    });
}


// Newsletter
if(document.forms.namedItem("newsletterForm")){
    let form = document.forms.namedItem("newsletterForm");
    form.addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        fetch(resturl + 'newsletter-rest', {
            method: 'POST',
            body: new FormData(form),
            headers: {'X-WP-Nonce': wpApiSettings.nonce},
            cache: 'no-cache',
        })
        .then(
            function(response) {
                if (response.status !== 200) { 
                    document.getElementById('responseNews').innerHTML = 'Cette adresse email est déjà enregistrée !';
                    if (document.getElementsByTagName("html")[0].lang == "en-GB") {
                        document.getElementById('responseNews').innerHTML = 'This email address is already registered !';
                    }
                    return;
                }
                response.json().then(function(data) {
                    document.getElementById('submitNews').style.display ='none';
                    document.getElementById('responseNews').innerHTML = 'Votre adresse email vient d\'être enregistrée !';
                    if (document.getElementsByTagName("html")[0].lang == "en-GB") {
                        document.getElementById('responseNews').innerHTML = 'Your email adress has been registered !';
                    }
                });
            }
        )
        .catch(function(err) {
            console.log('Fetch Error :-S', err);
        });
    });
}



// Replace SVG icons

/* let facebook = document.getElementsByClassName('wp-social-link-facebook')[0];
let svgFb = facebook.getElementsByTagName('a')[0];
svgFb.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="12.623" height="27.347" viewBox="0 0 12.623 27.347"><path id="Tracé_60" data-name="Tracé 60" d="M17.336,35.624h5.506V21.834h3.842l.41-4.617H22.842V14.588c0-1.089.219-1.519,1.272-1.519h2.98V8.277H23.28c-4.1,0-5.945,1.8-5.945,5.259v3.681H14.471v4.675h2.865Z" transform="translate(-14.471 -8.277)"/></svg>'


let linkedin = document.getElementsByClassName('wp-social-link-linkedin')[0];
let svgIn = linkedin.getElementsByTagName('a')[0];
svgIn.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="22.678" height="22.688" viewBox="0 0 22.678 22.688"><g id="Groupe_633" data-name="Groupe 633" transform="translate(-11.914 -10.016)"><rect id="Rectangle_13" data-name="Rectangle 13" width="4.682" height="15.125" transform="translate(12.364 17.578)"/><path id="Tracé_68" data-name="Tracé 68" d="M13.54,14.758a2.791,2.791,0,1,0-2.768-2.791,2.782,2.782,0,0,0,2.768,2.791" transform="translate(1.142 0.84)"/><path id="Tracé_69" data-name="Tracé 69" d="M22.1,22.8c0-2.127.978-3.393,2.853-3.393,1.721,0,2.549,1.216,2.549,3.393v7.939h4.66V21.16c0-4.051-2.3-6.01-5.5-6.01a5.272,5.272,0,0,0-4.557,2.5V15.611H17.608V30.736H22.1Z" transform="translate(2.432 1.967)"/></g></svg>'

 */




document.addEventListener('DOMContentLoaded', function () {
    if (document.getElementById('see-all-artists')) {
        var btnArtist = document.getElementById('see-all-artists');
        btnArtist.addEventListener('click', function (el) {
            document.getElementById('artistsList').innerHTML += htmlArt;
            document.getElementById('see-all-artists').style.display = 'none';
        });
    }

    
    if(document.body.offsetWidth < 600 && document.getElementsByClassName('wp-block-polylang-language-switcher')){
        let els = document.getElementsByClassName('wp-block-polylang-language-switcher');
        Array.prototype.forEach.call(els, function (el) {
            let As = el.getElementsByTagName('a');
            Array.prototype.forEach.call(As, function (a) {
                a.innerHTML = a.innerHTML.substr(0, 2);
            });
        });
    }

});





function artistLess() {
    let btnLess = document.createElement("button");
    btnLess.innerHTML = 'Voir moins d\'artistes';
    // See less artists
    if (document.getElementsByTagName("html")[0].lang == "en-GB") {
        btnLess.innerHTML = 'See less artists';
    }
    btnLess.id = 'see-less-artists';
    document.getElementsByClassName('entry-content')[0].appendChild(btnLess)
    document.getElementById('see-less-artists').addEventListener('click',function () {
        location.reload();
    })
}