<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Armando
 * @since 1.0.0
 */

/**
 * The theme version.
 *
 * @since 1.0.0
 */
define( 'IHAG_VERSION', wp_get_theme()->get( 'Version' ) );

/** Check if the WordPress version is 5.5 or higher, and if the PHP version is at least 7.2. If not, do not activate. */
if ( version_compare( $GLOBALS['wp_version'], '5.5', '<' ) || version_compare( PHP_VERSION_ID, '70200', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

if( function_exists('acf_add_options_page') ) {
	// Page principale
	acf_add_options_page(array(
		'page_title'    => 'Options',
		'menu_title'    => 'Options',
		'menu_slug'     => 'options-generales',
		'capability'    => 'edit_posts',
		'redirect'      => true
	));

  // Page d'options
  acf_add_options_sub_page(array(
  	'page_title' 	=> 'Options Générales',
  	'menu_slug' 	=> 'acf_options',
  	'parent_slug'   => 'options-generales'
  ));
}

/**
 * Adds theme-supports.
 *
 * @since 1.2.4
 * @return void
 */
function armando_setup() {
	// Add support for Block Styles.
	add_theme_support( 'wp-block-styles' );
	// Enqueue editor styles.
	add_theme_support( 'editor-styles' );

	add_theme_support( 'custom-units');

	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	add_editor_style(
/* 		array(
			'./assets/css/style-shared.css',
		) */
		"style.css"
	);


	/*
	* Enable support for Post Thumbnails on posts and pages.
	*
	* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	*/
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1568, 9999 );
	add_image_size( '150-150', 100, 100 );
	add_image_size( '350-350', 350, 350 );
	add_image_size( '450-450', 450, 450 );
	add_image_size( '650-650', 650, 650 );
	add_image_size( '850-850', 850, 850 );

	function ihag_custom_sizes( $sizes ) {
		return array_merge(
			$sizes,
			array(
				'150-150' => __( '150x150 - Petite sans rognage', 'ihag' ),
				'350-350' => __( '350x350 - Contact sans rognage', 'ihag' ),
				'450-450' => __( '450x450 - Moyen sans rognage', 'ihag' ),
				'650-650' => __( '650x650 - Grande sans rognage', 'ihag' ),
				'650-650' => __( '850x850 - Grande sans rognage', 'ihag' ),
			)
		);
	}
	add_filter( 'image_size_names_choose', 'ihag_custom_sizes' );


	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	// Add support for editor styles.
	add_theme_support( 'editor-styles' );
}
add_action( 'after_setup_theme', 'armando_setup' );

/**
 * Enqueue the style.css file.
 *
 * @since 1.0.0
 */
/* function armando_styles() {
	wp_enqueue_style(
		'armando-style',
		get_stylesheet_uri(),
		'',
		IHAG_VERSION
	);
	
	wp_enqueue_style(
		'armando-shared-stylez',
		get_theme_file_uri( 'assets/css/style-shared.css' ),
		'',
		IHAG_VERSION
	);
}
add_action( 'wp_enqueue_scripts', 'armando_styles' ); */

/**
 * Show '(No title)' if post has no title.
 */
add_filter(
	'the_title',
	function( $title ) {
		if ( ! is_admin() && empty( $title ) ) {
			$title = __( '(No title)', 'armando' );
		}

		return $title;
	}
);


add_action( 'init', 'ihag_custom_post_property' );
function ihag_custom_post_property() {

	register_post_type(
		'artists', 
		array(
			'labels'             => array(
				'name'          => __( 'Artistes', 'ihag' ),
				'singular_name' => __( 'Artiste', 'ihag' ),
			),
			'menu_position'      => 5,
			'menu_icon'          => 'dashicons-admin-appearance',
			'hierarchical'       => false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			/* 'rewrite'            => array( 'slug' => 'name' ), */
			'show_in_rest'       => false,
			'has_archive'        => false,
			'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' ),
		)
	);
	
	
	register_post_type(
		'news', 
		array(
			'labels'             => array(
				'name'          => __( 'Actualités', 'ihag' ),
				'singular_name' => __( 'Actualité', 'ihag' ),
			),
			'menu_position'      => 4,
			'menu_icon'          => 'dashicons-admin-appearance',
			'hierarchical'       => false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			/* 'rewrite'            => array( 'slug' => 'name' ), */
			'show_in_rest'       => true,
			'has_archive'        => true,
			'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' ),
		)
	);
	$args = array(
		'label'        => __( 'Catégories', 'ihag' ),
		'hierarchical' => true,
		'show_in_rest'       => true,
	);
	register_taxonomy( 'category_news', 'news', $args );
	
	
	/* 
	EXEMPLE STRUCTURE DÉCLARATION TAXO POUR CPT :

	$args = array(
		'label'        => __( 'taxo', 'ihag' ),
		'hierarchical' => true,
	);
	register_taxonomy( 'slug', 'cpt', $args );
	*/
}

function post_remove ()      //creating functions post_remove for removing menu item
{ 
   remove_menu_page('edit.php');
}
add_action('admin_menu', 'post_remove'); 

function myplugin_register_template() {
    $post_type_object = get_post_type_object( 'post' );
    $post_type_object->template = array(
        array( 'core/image' ),
    );
}
add_action( 'init', 'myplugin_register_template' );

/**
 * Propose les plugins à télécharger.
 */
require get_template_directory() . '/inc/plugin-require.php';


/**
 * REST API
 */
require get_template_directory() . '/functions-rest-api.php';


// Variantes de styles
function ihag_register_styles() {
	/*
	EXEMPLE STRUCTURE DÉCLARATION STYLE DE BLOC GUT :

	register_block_style(
		'core/bloc',
		array(
			'name'	        => 'slug',
			'label'	        => 'name'
		)
	); 
	*/

}

add_action( 'init', 'ihag_register_styles' );

/**
 *  ACF Field.
 */
require get_template_directory() . '/inc/acf-block.php';

/**
 *  ACF Field styles.
 */
require get_template_directory() . '/inc/acf-style.php';

/**
 * Enqueue scripts and styles.
 */
function ihag_scripts() {
	wp_enqueue_style( 'ihag-style', get_stylesheet_uri(), array(), IHAG_VERSION );

	wp_enqueue_script( 'ihag-script', get_template_directory_uri() . '/js/script.js', array(), IHAG_VERSION, true );
	wp_enqueue_script( 'ihag-modale', get_template_directory_uri() . '/js/modale.js', array(), IHAG_VERSION, true );
	wp_localize_script('ihag-script', 'resturl', array(site_url() . '/wp-json/ihag/'));
	wp_localize_script( 'ihag-script', 'wpApiSettings', array(
		'root' => esc_url_raw( rest_url() ),
		'nonce' => wp_create_nonce( 'wp_rest' )
	) );

}
add_action( 'wp_enqueue_scripts', 'ihag_scripts' );

/**
 * My_acf_json_save_point
 *
 * @param  mixed $path
 * @return string
 */
function ihag_acf_json_save_point( $path ) {
	$path = get_stylesheet_directory() . '/jsonACF';
	if ( ! file_exists( $path ) ) {
		mkdir( $path, 0777 );}
	return $path;
}
add_filter( 'acf/settings/save_json', 'ihag_acf_json_save_point' );


/**
 * My_acf_json_load_point
 *
 * @param  mixed $paths
 * @return string
 */
function ihag_acf_json_load_point( $paths ) {
	unset( $paths[0] );
	$paths[] = get_stylesheet_directory() . '/jsonACF';
	return $paths;
}
add_filter( 'acf/settings/load_json', 'ihag_acf_json_load_point' );


if ( ! defined( 'WP_POST_REVISIONS' ) ) {
	define( 'WP_POST_REVISIONS', 3 );
}


/**
 * Ihag_revision_number
 *
 * @param  mixed $num
 * @param  mixed $post
 * @return int
 */
function ihag_revision_number( $num, $post ) {
	return 3;
}
add_filter( 'wp_revisions_to_keep', 'ihag_revision_number', 4, 2 );

// un intervalle entre deux sauvegardes de 360 secondes.
if ( ! defined( 'AUTOSAVE_INTERVAL' ) ) {
	define( 'AUTOSAVE_INTERVAL', 360 );
}

/**
 * Ihag_clean_head
 * 
 * @link https://crunchify.com/how-to-clean-up-wordpress-header-section-without-any-plugin/
 * 
 * @return void
 */
function ihag_clean_head() {
	return '';
}
add_filter( 'the_generator', 'ihag_clean_head' );

remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head' );
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds.
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed.


/* add_filter('register_post_type_args', 'add_archive_post', 10, 2);
function add_archive_post($args, $post_type){
    if ($post_type == 'post'){
        $args['has_archive'] = true;
		$args['rewrite']['slug'] = __('actualite', 'ihag');
		$args['rewrite']['with_front'] = false;
    }
    return $args;
} */

/* function post_archive_link(){
	$post_type_obj = get_post_type_object( 'post' );
	$struct = $wp_rewrite->root . $post_type_obj->rewrite['slug'];
	$link = home_url( user_trailingslashit( $struct, 'post_type_archive' ) );
	return $link;
} */

/**
 * Hide email from Spam Bots using a shortcode.
 *
 * @param array  $atts    Shortcode attributes. Not used.
 * @param string $content The shortcode content. Should be an email address.
 * @return string The obfuscated email address. 
 */
function wpdocs_hide_email_shortcode( $atts , $content = null ) {
    if ( ! is_email( $content ) ) {
        return;
    }
    return '<a target="_blank" href="' . esc_url('mailto:' . antispambot( $content, 1 ) ) . '">' . esc_html( antispambot( $content ) ) . '</a>';
}
add_shortcode( 'email', 'wpdocs_hide_email_shortcode' );


//Remove Post
	/* function post_remove ()   
	{
	remove_menu_page('edit.php');
	}
	add_action('admin_menu', 'post_remove'); */




//gestion pattern 
add_action('init', function() {
	remove_theme_support('core-block-patterns');
});

function my_plugin_register_my_pattern_categories() {
	register_block_pattern_category(
		'team',
		array( 'label' => __( 'Page équipe', 'ihag' ) )
	);
	register_block_pattern_category(
		'global',
		array( 'label' => __( 'Bloc généraux', 'ihag' ) )
	);
	register_block_pattern_category(
		'news',
		array( 'label' => __( 'Actualités', 'ihag' ) )
	);
	register_block_pattern_category(
		'contact',
		array( 'label' => __( 'Contact', 'ihag' ) )
	);
	register_block_pattern_category(
		'collection',
		array( 'label' => __( 'Greenline Collection', 'ihag' ) )
	);
	register_block_pattern_category(
		'hmv',
		array( 'label' => __( 'Histoire mission et valeurs', 'ihag' ) )
	);
	register_block_pattern_category(
		'info',
		array( 'label' => __( 'Je m\'informe', 'ihag' ) )
	);
	register_block_pattern_category(
		'power',
		array( 'label' => __( 'La puissance de l\'art', 'ihag' ) )
	);
	register_block_pattern_category(
		'projet',
		array( 'label' => __( 'Les projets terrain', 'ihag' ) )
	);
}
add_action( 'init', 'my_plugin_register_my_pattern_categories' );

//Load template part
function load_template_part($template_name, $part_name=null) {
    ob_start();
    get_template_part($template_name, $part_name);
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
}