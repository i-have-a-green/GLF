<?php
/**
 * Title: Bandeau coloré avec logo GLF
 * Slug: ihag/colored-band-logo
 * Categories:  global
 */
?>

<!-- wp:group {"align":"full","backgroundColor":"color__yellow","layout":{"inherit":true}} -->
<div class="wp-block-group alignfull has-color-yellow-background-color has-background"><!-- wp:spacer {"height":"8px"} -->
<div style="height:8px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"className":"is-style-logo-outtopright"} -->
<div class="wp-block-group is-style-logo-outtopright"><!-- wp:heading {"textAlign":"center"} -->
<h2 class="has-text-align-center"></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"16px"} -->
<div style="height:16px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer --></div>
<!-- /wp:group -->