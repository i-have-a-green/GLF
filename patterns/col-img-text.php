<?php
/**
 * Title: Colonnes Image & texte
 * Slug: ihag/col-img-text
 * Categories:  global
 */
?>

<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:image {"align":"center","className":"is-style-caption-left"} -->
<figure class="wp-block-image aligncenter is-style-caption-left"><img alt=""/></figure>
<!-- /wp:image --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center","layout":{"inherit":false,"contentSize":"22rem"}} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:heading -->
<h2></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->