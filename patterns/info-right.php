<?php
/**
 * Title: groupe image droite
 * Slug: ihag/info-right
 * Categories:  info
 */
?>
<!-- wp:columns {"align":"wide","style":{"spacing":{"blockGap":"3rem"}}} -->
<div class="wp-block-columns alignwide"><!-- wp:column {"width":"66.66%"} -->
<div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:heading -->
<h2>Le stockage carbone</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Les forêts constituent avec les océans les principaux pièges à carbone de la Terre. Les arbres, la matière organique et les sols captent du CO<sub>2</sub> tout au long de leur vie. Les forêts ont ainsi un fort potentiel pour lutter contre le réchauffement climatique en cours et à venir. Elles sont donc devenues un levier de verdissement pour des structures prétendant compenser leurs émissions carbone.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Depuis 2005, le protocole de Kyoto a formalisé les approches de compensation des activités humaines vis-à-vis de l’environnement. Le principe est le suivant : une structure qui émet des gaz à effet de serre (exprimé en équivalent CO<sub>2</sub>) devrait promouvoir des activités qui stockent du CO<sub>2</sub> afin de compenser leurs émissions. Pour le dire simplement, si l’on suit cette théorie : 1 (une émission) - 1 (une compensation) = 0 (je n’ai pas pollué).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Cette attention intéressée portée sur les forêts donne ainsi lieu à des projets plus ou moins bénéfiques du point de vue de l’environnement. Planter des arbres n’est pas systématiquement une bonne mesure écologique. En effet, planter en monoculture des arbres d’une même espèce et importée, c’est négliger l’équilibre physico-chimique du milieu, la biodiversité et les populations locales et pourtant cela ne déroge pas à des prérogatives de compensation carbone. C’est justement croire que tout cela est aussi simple que 1-1 = 0.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Plus largement, les logiques de compensation sont parfois des freins à des modifications plus profondes des structures émissives. Puisque je compense, pourquoi réduire ? Le CO<sub>2</sub> le moins nuisible reste pourtant celui qui n’est pas émis et non celui qui est soit-disant compensé.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dans ce contexte, Greenline Foundation appelle à la vigilance quant aux allégations et approches liées aux projet forestiers. Nous soutenons des projets « holistiques » qui prennent en compte l’ensemble des facteurs du milieu (ressources, biodiversité, souveraineté et conditions de vie des population, etc) et pour lesquels le stockage carbone apparait comme un co-bénéfice d’une gestion durable totale.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"33.33%"} -->
<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:group {"backgroundColor":"color__salmon","layout":{"inherit":false}} -->
<div class="wp-block-group has-color-salmon-background-color has-background"><!-- wp:heading {"level":3} -->
<h3>Répartition du stockage du carbone dans une forêt, 2020</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"className":"is-style-source"} -->
<p class="is-style-source">source : FAO, Evaluation des ressources forestières mondiales, 2020, rapport principal, p15</p>
<!-- /wp:paragraph -->

<!-- wp:acf/modal-image {"id":"block_62f25567ef950","name":"acf/modal-image","data":{"image":660,"_image":"field_62f22d35da193"},"align":"full","mode":"auto"} /--></div>
<!-- /wp:group -->

<!-- wp:group {"backgroundColor":"color__darkyellow","layout":{"inherit":false}} -->
<div class="wp-block-group has-color-darkyellow-background-color has-background"><!-- wp:heading {"level":3} -->
<h3>Pour en savoir plus sur les questions carbone et les allégations à la neutralité :</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"className":"is-style-link"} -->
<p class="is-style-link"><a href="https://all4trees.org/comprendre/" target="_blank" rel="noreferrer noopener">Découvrez All4trees</a><br><a href="https://www.carbone4.com/analysis" target="_blank" rel="noreferrer noopener">Découvrez Carbone 4</a><br><a href="https://envol-vert.org/la-foret/" target="_blank" rel="noreferrer noopener">Découvrez Envol Vert</a><br><a href="https://librairie.ademe.fr/developpement-durable/5335-utilisation-de-l-argument-de-neutralite-carbone-dans-les-communications.html" target="_blank" rel="noreferrer noopener">Découvrez l'ADEME</a></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->