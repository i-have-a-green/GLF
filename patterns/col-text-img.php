<?php
/**
 * Title: Colonnes Texte & Image
 * Slug: ihag/col-text-img
 * Categories:  global
 */
?>

<!-- wp:columns {"align":"wide","className":"reverse-mobile"} -->
<div class="wp-block-columns alignwide reverse-mobile"><!-- wp:column {"verticalAlignment":"center","layout":{"inherit":false,"contentSize":"22rem"}} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:heading -->
<h2></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:image {"align":"center","className":"is-style-caption-right"} -->
<figure class="wp-block-image aligncenter is-style-caption-right"><img alt=""/></figure>
<!-- /wp:image --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->