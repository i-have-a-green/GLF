<?php
/**
 * Title: Groupe coordonnées
 * Slug: ihag/contact-coord
 * Categories:  contact
 */
?>
<!-- wp:heading {"textAlign":"center"} -->
<h2 class="has-text-align-center">Nos coordonnées</h2>
<!-- /wp:heading -->

<!-- wp:group {"align":"wide"} -->
<div class="wp-block-group alignwide"><!-- wp:group -->
<div class="wp-block-group"><!-- wp:group {"backgroundColor":"color__lightgreen","className":"is-style-logo-topright"} -->
<div class="wp-block-group is-style-logo-topright has-color-lightgreen-background-color has-background"><!-- wp:heading {"textAlign":"center","level":3} -->
<h3 class="has-text-align-center">Notre téléphone</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><a href="tel:+33617507422">+33 6 17 50 74 22</a></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"backgroundColor":"color__salmon","layout":{"inherit":true}} -->
<div class="wp-block-group has-color-salmon-background-color has-background"><!-- wp:heading {"textAlign":"center","level":3} -->
<h3 class="has-text-align-center">Nos adresses</h3>
<!-- /wp:heading -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:paragraph {"align":"center","fontSize":"medium"} -->
<p class="has-text-align-center has-medium-font-size"><strong>Siège social</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center","fontSize":"extra-small"} -->
<p class="has-text-align-center has-extra-small-font-size">Greenline Foundation<br>21 rue de Paris<br>91370 Verrières-le-Buisson</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:paragraph {"align":"center","fontSize":"medium"} -->
<p class="has-text-align-center has-medium-font-size"><strong>Bureaux</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">Greenline Foundation<br>8 rue Magellan<br>75008 Paris</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->

<!-- wp:group {"backgroundColor":"color__green"} -->
<div class="wp-block-group has-color-green-background-color has-background"><!-- wp:heading {"textAlign":"center","level":3} -->
<h3 class="has-text-align-center">Notre courriel</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">[email]contact@greenline.foundation[/email]</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->