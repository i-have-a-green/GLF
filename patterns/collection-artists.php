<?php
/**
 * Title: Listing artistes
 * Slug: ihag/collection-artists
 * Categories:  collection
 */
?>
<!-- wp:heading {"textAlign":"center"} -->
<h2 class="has-text-align-center">Les artistes</h2>
<!-- /wp:heading -->

<!-- wp:acf/listing-artists {"id":"block_62ea73ffa62c3","name":"acf/listing-artists","align":"wide","mode":"preview"} /-->