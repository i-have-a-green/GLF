<?php
/**
 * Title: Citation présidente
 * Slug: ihag/cite-pres
 * Categories:  hmv
 */
?>
<!-- wp:group {"align":"wide"} -->
<div class="wp-block-group alignwide"><!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column {"width":"33.33%"} -->
<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:image {"id":125,"width":311,"height":522,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://greenline.ihaveagreen.fr/wp-content/uploads/2022/08/Sarah_VALENTE.jpg" alt="" class="wp-image-125" width="311" height="522"/></figure>
<!-- /wp:image --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"66.66%"} -->
<div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:quote {"backgroundColor":"color__yellow","className":"is-style-citation-right"} -->
<blockquote class="wp-block-quote is-style-citation-right has-color-yellow-background-color has-background"><p>Enfant, je me rappelle<br>J’avais les pieds nus et je courrais dans l’herbe.<br>Je m’agrippais aux branches et contemplait sereine, les haleurs du soir.<br>Je me sentais grande, bien plus grande que mon corps,<br>Bien plus grande que ces adultes qui, pour quelques bêtises, ont détruit tant de rêves.<br>Enfant, je me sentais invincible, immortelle, généreuse.<br>Je crois qu’enfant je me sentais moi-même.<br>Moi-même, comme la forêt, fidèle à ses racines depuis la nuit des temps.<br>Et depuis j’ai grandi et je vois les hommes se battre mais je n’ai jamais vu les arbres s’entretuer.</p><cite>Sarah Valente</cite></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->