<?php
/**
 * Title: Bloc d'affichage d'actualité x3
 * Slug: ihag/news-3
 * Categories:  news
 */
?>
<!-- wp:group {"align":"full","backgroundColor":"color__yellow","className":"is-style-default"} -->
<div class="wp-block-group alignfull is-style-default has-color-yellow-background-color has-background"><!-- wp:heading {"textAlign":"center"} -->
<h2 class="has-text-align-center">Actualités</h2>
<!-- /wp:heading -->

<!-- wp:acf/listing-article {"id":"block_62dfb2c75cd0e","name":"acf/listing-article","data":{},"align":"","mode":"preview"} /--></div>
<!-- /wp:group -->