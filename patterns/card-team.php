<?php
/**
 * Title: Carte d'équipe
 * Slug: ihag/card-team
 * Categories:  team
 */
?>

<!-- wp:group {"className":"team-card"} -->
<div class="wp-block-group team-card"><!-- wp:image {"align":"center","id":181,"sizeSlug":"large","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-large"><img src="http://greenline-foundation.local/wp-content/uploads/2022/08/carre.svg" alt="" class="wp-image-181"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"textAlign":"center","level":3} -->
<h3 class="has-text-align-center"></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->