<?php
/**
 * Title: Listing colonnes *4
 * Slug: ihag/hmv-cards-4
 * Categories:  hmv
 */
?>
<!-- wp:group {"align":"full","backgroundColor":"color__salmon","className":"is-style-default"} -->
<div class="wp-block-group alignfull is-style-default has-color-salmon-background-color has-background"><!-- wp:heading {"textAlign":"center"} -->
<h2 class="has-text-align-center">Nos valeurs</h2>
<!-- /wp:heading -->

<!-- wp:group {"layout":{"wideSize":"1000px"}} -->
<div class="wp-block-group"><!-- wp:columns {"className":"is-style-card-values"} -->
<div class="wp-block-columns is-style-card-values"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:image {"id":99,"sizeSlug":"large","linkDestination":"none","className":"is-style-default"} -->
<figure class="wp-block-image size-large is-style-default"><img src="https://greenline.ihaveagreen.fr/wp-content/uploads/2022/07/Picto_Feuilles-03.svg" alt="" class="wp-image-99"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"textAlign":"center","level":3} -->
<h3 class="has-text-align-center">Eveil</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">GLF cultive une attention sensible à la nature, à sa beauté et à son rôle critique. Nous diffusons cette prise de conscience et la prolongeons en une action concrète de préservation.<br>L’éducation et l’information sont ainsi au cœur de nos actions.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:image {"id":114,"sizeSlug":"large","linkDestination":"none"} -->
<figure class="wp-block-image size-large"><img src="https://greenline.ihaveagreen.fr/wp-content/uploads/2022/08/Picto_Fleur-02.svg" alt="" class="wp-image-114"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"textAlign":"center","level":3} -->
<h3 class="has-text-align-center">Création</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">L’Art est une puissance de création et de transmission. Grâce à lui GLF explore et diffuse des rapports au végétal inédits et de nouvelles solutions. La créativité est ainsi une réponse aux forces de destruction qui pèsent sur les écosystèmes forestiers du monde.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:image {"id":115,"sizeSlug":"large","linkDestination":"none"} -->
<figure class="wp-block-image size-large"><img src="https://greenline.ihaveagreen.fr/wp-content/uploads/2022/08/Picto_Coeur-Feuille.svg" alt="" class="wp-image-115"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"textAlign":"center","level":3} -->
<h3 class="has-text-align-center">Réconciliation</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">A revers de l’idée d’un fossé irréconciliable, nous œuvrons pour une reconnexion de l’homme au monde vivant pour mieux comprendre ses enjeux. L’art est porteur de cohérence et de beauté, il préfigure l’union que nous voulons élaborer entre l’Homme et la Nature.&nbsp;</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:image {"id":116,"sizeSlug":"large","linkDestination":"none"} -->
<figure class="wp-block-image size-large"><img src="https://greenline.ihaveagreen.fr/wp-content/uploads/2022/08/Picto_Main-Feuilles.svg" alt="" class="wp-image-116"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"textAlign":"center","level":3} -->
<h3 class="has-text-align-center">Respect</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">Nous demeurons attentifs et sensibles aux grandeurs et aux fragilités du monde qui nous entourent. En contact avec des écosystèmes de toutes échelles et avec les populations locales, GLF œuvre pour la juste valorisation et l’estime de l’autre. </p>
<!-- /wp:paragraph --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"24px"} -->
<div style="height:24px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer --></div>
<!-- /wp:group -->