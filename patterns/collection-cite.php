<?php
/**
 * Title: Groupe citation *3
 * Slug: ihag/collection-cite-3
 * Categories:  collection
 */
?>
<!-- wp:columns {"isStackedOnMobile":false,"align":"wide","className":"is-style-citation-small"} -->
<div class="wp-block-columns alignwide is-not-stacked-on-mobile is-style-citation-small"><!-- wp:column {"width":"15%"} -->
<div class="wp-block-column" style="flex-basis:15%"><!-- wp:image {"id":274,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="https://greenline.ihaveagreen.fr/wp-content/uploads/2022/08/Eva_JOSPIN-2.jpg" alt="" class="wp-image-274"/></figure>
<!-- /wp:image --></div>
<!-- /wp:column -->

<!-- wp:column {"width":""} -->
<div class="wp-block-column"><!-- wp:quote {"backgroundColor":"color__salmon","className":"is-style-citation-small-right"} -->
<blockquote class="wp-block-quote is-style-citation-small-right has-color-salmon-background-color has-background"><p>Nous ne sommes pas séparés de la nature, nous sommes dans le vivant et nous subissons toutes les conséquences de ce que nous faisons subir au vivant.</p><cite>Eva Jospin<br>Artiste plasticienne</cite></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"isStackedOnMobile":false,"align":"wide","className":"is-style-citation-small"} -->
<div class="wp-block-columns alignwide is-not-stacked-on-mobile is-style-citation-small"><!-- wp:column {"width":""} -->
<div class="wp-block-column"><!-- wp:quote {"backgroundColor":"color__green","className":"is-style-citation-small-left"} -->
<blockquote class="wp-block-quote is-style-citation-small-left has-color-green-background-color has-background"><p>Mes réalisations sont un moyen de parler du temps long, en opposition au “temps de l’Homme”. Je souhaite faire le lien entre la Terre dans son entièreté et ce que les non-scientifiques perçoivent de cette planète.</p><cite>Noémie Goudal<br>Artiste plasticienne</cite></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"15%"} -->
<div class="wp-block-column" style="flex-basis:15%"><!-- wp:image {"id":275,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="https://greenline.ihaveagreen.fr/wp-content/uploads/2022/08/Noemie_GOUDAL-1.jpg" alt="" class="wp-image-275"/></figure>
<!-- /wp:image --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"isStackedOnMobile":false,"align":"wide","className":"is-style-citation-small"} -->
<div class="wp-block-columns alignwide is-not-stacked-on-mobile is-style-citation-small"><!-- wp:column {"width":"15%"} -->
<div class="wp-block-column" style="flex-basis:15%"><!-- wp:image {"id":276,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="https://greenline.ihaveagreen.fr/wp-content/uploads/2022/08/Bruno_GADENNE-1.jpg" alt="" class="wp-image-276"/></figure>
<!-- /wp:image --></div>
<!-- /wp:column -->

<!-- wp:column {"width":""} -->
<div class="wp-block-column"><!-- wp:quote {"backgroundColor":"color__yellow","className":"is-style-citation-small-right"} -->
<blockquote class="wp-block-quote is-style-citation-small-right has-color-yellow-background-color has-background"><p>La nature a quelque chose d’universel, d’intemporel qui permet à chacun de se l’approprier. L’Homme a un rapport inné de frayeur et d’admiration vis-à-vis de celle-ci.</p><cite>Bruno Gadenne<br>Artiste peintre</cite></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->