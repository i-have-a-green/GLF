<?php
/**
 * Title: Groupe image gauche
 * Slug: ihag/info-left
 * Categories:  info
 */
?>
<!-- wp:columns {"align":"wide","style":{"spacing":{"blockGap":"3rem"}},"className":"reverse-mobile"} -->
<div class="wp-block-columns alignwide reverse-mobile"><!-- wp:column {"width":"33.33%"} -->
<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:group {"backgroundColor":"color__green","layout":{"inherit":false}} -->
<div class="wp-block-group has-color-green-background-color has-background"><!-- wp:heading {"level":3} -->
<h3>Les principaux fronts de déforestation</h3>
<!-- /wp:heading -->

<!-- wp:acf/modal-image {"id":"block_62f25574ef951","name":"acf/modal-image","data":{"image":284,"_image":"field_62f22d35da193"},"align":"full","mode":"auto"} /-->

<!-- wp:paragraph {"className":"is-style-source"} -->
<p class="is-style-source">source : WWF, les fronts de déforestation, 2021, p 6-7</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:image {"id":285,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="https://greenline.ihaveagreen.fr/wp-content/uploads/2022/08/Noemie_GOUDAL_2.jpg" alt="" class="wp-image-285"/></figure>
<!-- /wp:image --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"66.66%"} -->
<div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:heading -->
<h2>Les risques et menaces</h2>
<!-- /wp:heading -->

<!-- wp:heading {"level":3} -->
<h3>Entre 2015 et 2020, on estime à 10 millions d’ha la surface de forêt perdue par an. Cette perte est due à plusieurs causes :</h3>
<!-- /wp:heading -->

<!-- wp:list {"className":"is-style-colorful-bulletpoint"} -->
<ul class="is-style-colorful-bulletpoint"><li>L’agriculture (80%) dont l’agriculture commerciale et industrielle (45-50%), l’agriculture de subsistance (30-35%) et l’élevage (14%) ;</li><li>Les infrastructures et l’expansion urbaine (13%) ;</li><li>Les activités minières (6%).</li></ul>
<!-- /wp:list -->

<!-- wp:spacer {"height":"16px"} -->
<div style="height:16px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:heading {"level":3} -->
<h3>A cette déforestation, s’ajoute un phénomène de malforestation :</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>La standardisation des bois, les champs d’arbres et l’essor des forêts silencieuses sans biodiversité ne représentent ainsi pas à proprement parler une perte quantitative de surface de forêt mais bien une immense perte qualitative à tout autre point de vue. Ainsi, en France, 80% des plantations réalisées sont des monocultures.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>En outre, le réchauffement climatique augure une forte pression sur les espèces forestières et nécessitent de penser des modèles de gestion résiliente et durable (voir par exemple la forêt mosaïque).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Le recul des forêts n’a pourtant rien d’irrémédiable et ne doit pas donner naissance à une immobilité fataliste. Ces dernières années et grâce aux combats acharnés d’activistes et d’acteurs du secteur on remarque une relative baisse du taux annuel de déforestation : 10 millions d’ha entre 2015-2020 au lieu de 12 millions d’ha entre 2010-2015.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->