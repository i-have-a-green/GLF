<?php
/**
 * Title: Groupe image gauche & fond couleur selectionnable
 * Slug: ihag/visual-left
 * Categories:  power
 */
?>
<!-- wp:group {"align":"wide"} -->
<div class="wp-block-group alignwide"><!-- wp:columns {"className":"is-style-default col-padd"} -->
<div class="wp-block-columns is-style-default col-padd"><!-- wp:column {"width":"33.33%"} -->
<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:image {"id":294,"sizeSlug":"full","linkDestination":"none","className":"is-style-caption-right"} -->
<figure class="wp-block-image size-full is-style-caption-right"><img src="https://greenline.ihaveagreen.fr/wp-content/uploads/2022/08/Romain_BERNINI-1.jpg" alt="" class="wp-image-294"/><figcaption>Him - Détail - Romain BERNINI</figcaption></figure>
<!-- /wp:image --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"66.66%","backgroundColor":"color__salmon","className":"is-style-bg-logo-text-img"} -->
<div class="wp-block-column is-style-bg-logo-text-img has-color-salmon-background-color has-background" style="flex-basis:66.66%"><!-- wp:group -->
<div class="wp-block-group"><!-- wp:heading {"level":3} -->
<h3>GLF initie des manifestations artistiques afin de sensibiliser la société à la cause environnementale en permettant de redécouvrir la beauté et le rôle critique<br>de la nature, et plus spécifiquement<br>des forêts.</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>L’art est un vecteur puissant d’émotion et nous croyons à sa pertinence environnementale. Assurant une cohérence entre la tête, le cœur et le corps, la création artistique sait mobiliser et motiver en une action concrète ceux qu’elle touche.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A revers d’une approche culpabilisatrice ou au contraire d’un optimisme technologique démesuré, GLF choisit de promouvoir une écologie culturelle et de solliciter ainsi l’imaginaire pour faire face aux grands enjeux de demain.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Plus d’une fois, les artistes ont prêté leurs talents à la préservation des forêts qui leur étaient chères. Les Artistes de Barbizon, pour ne citer qu’eux, ont été à l’initiative de la première aire protégée en France et dans le monde. S’inscrivant dans cette tradition, GLF mène ainsi en propre des projets culturels et artistiques pour sensibiliser tous les publics à la cause des forêts.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->