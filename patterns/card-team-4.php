<?php
/**
 * Title: Liste carte d'équipe x 4
 * Slug: ihag/card-team-liste-4
 * Categories:  team
 */
?>

<!-- wp:columns {"align":"wide","className":"list-team"} -->
<div class="wp-block-columns alignwide list-team"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"className":"team-card"} -->
<div class="wp-block-group team-card"><!-- wp:image {"align":"center","id":181,"sizeSlug":"large","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-large"><img src="http://greenline-foundation.local/wp-content/uploads/2022/08/carre.svg" alt="" class="wp-image-181"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"textAlign":"center","level":3} -->
<h3 class="has-text-align-center"></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"className":"team-card"} -->
<div class="wp-block-group team-card"><!-- wp:image {"align":"center","id":181,"sizeSlug":"large","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-large"><img src="http://greenline-foundation.local/wp-content/uploads/2022/08/carre.svg" alt="" class="wp-image-181"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"textAlign":"center","level":3} -->
<h3 class="has-text-align-center"></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"className":"team-card"} -->
<div class="wp-block-group team-card"><!-- wp:image {"align":"center","id":181,"sizeSlug":"large","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-large"><img src="http://greenline-foundation.local/wp-content/uploads/2022/08/carre.svg" alt="" class="wp-image-181"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"textAlign":"center","level":3} -->
<h3 class="has-text-align-center"></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"className":"team-card"} -->
<div class="wp-block-group team-card"><!-- wp:image {"align":"center","id":181,"sizeSlug":"large","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-large"><img src="http://greenline-foundation.local/wp-content/uploads/2022/08/carre.svg" alt="" class="wp-image-181"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"textAlign":"center","level":3} -->
<h3 class="has-text-align-center"></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->