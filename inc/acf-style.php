<?php
/**
 * Ihag_acf_styles
 *
 */

function ihag_register_acf_styles() {

		/*   
		EXEMPLE STRUCTURE STYLE BLOC CUSTOM ACF :
		
		register_block_style(
			'acf/le-slug-bloc',
			array(
				'name'	        => 'Slug du style pour class',
				'label'	        => 'Nom du style back office'
			)
		); 
		*/
		register_block_style(
			'core/separator',
			array(
				'name'         => 'leaf-sep',
				'label'        => __( 'Pointillé avec feuille', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/separator',
			array(
				'name'         => 'noleaf-sep',
				'label'        => __( 'Pointillé sans feuille', 'ihag' ),
				'is_default'   => true
			)
		);
		register_block_style(
			'core/separator',
			array(
				'name'         => 'logo-sep',
				'label'        => __( 'Pointillé logo', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/heading',
			array(
				'name'         => 'logo-left',
				'label'        => __( 'Arbre gauche titre', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/paragraph',
			array(
				'name'         => 'link',
				'label'        => __( 'lien avec flèche', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/paragraph',
			array(
				'name'         => 'source',
				'label'        => __( 'Source', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/image',
			array(
				'name'         => 'caption-left',
				'label'        => __( 'Info gauche', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/image',
			array(
				'name'         => 'caption-right',
				'label'        => __( 'Info droite', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/columns',
			array(
				'name'         => 'demarche',
				'label'        => __( 'Bloc démarche', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/columns',
			array(
				'name'         => 'citation-small',
				'label'        => __( 'Citation petite img', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/columns',
			array(
				'name'         => 'card-values',
				'label'        => __( 'Cartes valeurs', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/column',
			array(
				'name'         => 'bg-logo-text-img',
				'label'        => __( 'Img + texte avec background et logo', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/column',
			array(
				'name'         => 'bg-no-logo-text-img',
				'label'        => __( 'Img + texte avec background sans logo', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/quote',
			array(
				'name'         => 'citation-right',
				'label'        => __( 'Citation à droite', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/quote',
			array(
				'name'         => 'citation-left',
				'label'        => __( 'Citation à gauche', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/quote',
			array(
				'name'         => 'citation-small-right',
				'label'        => __( 'Citation à droite petite image' , 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/quote',
			array(
				'name'         => 'citation-small-left',
				'label'        => __( 'Citation à gauche petite image', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/list',
			array(
				'name'         => 'colorful-bulletpoint',
				'label'        => __( 'Colorée', 'ihag' ),
				'is_default'   => false
			)
		);


		//group block
		register_block_style(
			'core/group',
			array(
				'name'         => 'logo-topright',
				'label'        => __( 'Logo Haut droite', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/group',
			array(
				'name'         => 'logo-outtopright',
				'label'        => __( 'Logo Haut droite exterieur', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/group',
			array(
				'name'         => 'project-right',
				'label'        => __( 'Groupe projet droite', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/group',
			array(
				'name'         => 'project-left',
				'label'        => __( 'Groupe projet gauche', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/group',
			array(
				'name'         => 'logo-botright',
				'label'        => __( 'Logo bas droite', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/group',
			array(
				'name'         => 'logo-botleft',
				'label'        => __( 'Logo bas gauche', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/group',
			array(
				'name'         => 'logo-topleft',
				'label'        => __( 'Logo haut gauche', 'ihag' ),
				'is_default'   => false
			)
		);
/* 		register_block_style(
			'core/group',
			array(
				'name'         => 'y-bg-big',
				'label'        => __( 'BG Jaune Grand', 'ihag' ),
				'is_default'   => false
			)
		);
		register_block_style(
			'core/group',
			array(
				'name'         => 's-bg-big',
				'label'        => __( 'BG Saumon Grand', 'ihag' ),
				'is_default'   => false
			)
		); */
}

add_action( 'init', 'ihag_register_acf_styles' );
?>