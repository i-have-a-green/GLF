<?php

add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {
        
        
        /*  
        EXEMPLE STRUCTURE BLOC CUSTOM ACF :

        acf_register_block_type(
            array(
                'name'				    => 'Slug du bloc',
                'title'				    => __('Nom back office'),
                'description'		    => __('Description back office'),
                'render_template'	    => 'template-parts/block/le-block.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                'enqueue_script'        => get_template_directory_uri() . '/js/le-block.js',
                'keywords'			    => array(
                                            '',
                                            '',
                                            ''
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
        */
        acf_register_block_type(
            array(
                'name'				    => 'hero_global',
                'title'				    => __('Bloc d\'entête général'),
                'description'		    => __(''),
                'render_template'	    => 'parts/block/hero.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                'keywords'			    => array(
                                            'hero',
                                            'header',
                                            'image'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'listing_article',
                'title'				    => __('Listing article'),
                'description'		    => __(''),
                'render_template'	    => 'parts/block/listing-article.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                'keywords'			    => array(
                                            'article',
                                            'home',
                                            'bloc'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'listing_artists',
                'title'				    => __('Listing des artistes'),
                'description'		    => __(''),
                'render_template'	    => 'parts/block/listing-artists.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                'keywords'			    => array(
                                            'article',
                                            'home',
                                            'bloc'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'newsletter',
                'title'				    => __('Inscriptions newsletter'),
                'description'		    => __(''),
                'render_template'	    => 'parts/block/newsletter.php',
                'mode'                  => 'preview',
                'icon'				    => 'pressthis',
                'keywords'			    => array(
                                            'news',
                                            'letter',
                                            'newsletter'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'ressources-like',
                'title'				    => __('Carte ressources custom'),
                'description'		    => __(''),
                'render_template'	    => 'parts/block/content-res-like-card.php',
                'align' => 'full',
                'mode'                  => 'auto',
                'icon'				    => 'pressthis',
                'keywords'			    => array(
                                            'news',
                                            'letter',
                                            'newsletter'
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'contact-form',
                'title'				    => __('Formulaire de contact'),
                'description'		    => __(''),
                'render_template'	    => 'parts/block/contact-form.php',
                'align' => 'full',
                'mode'                  => 'auto',
                'icon'				    => 'pressthis',
                'keywords'			    => array(
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'bottom-single-nav',
                'title'				    => __('Navigation article'),
                'description'		    => __(''),
                'render_template'	    => 'parts/block/bottom-single-nav.php',
                'align' => 'full',
                'mode'                  => 'auto',
                'icon'				    => 'pressthis',
                'keywords'			    => array(
                ),
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'modal-image',
                'title'				    => __('Modale zoom'),
                'description'		    => __(''),
                'render_template'	    => 'parts/block/modal-image.php',
                'align' => 'full',
                'mode'                  => 'auto',
                'icon'				    => 'pressthis',
                'supports'	            => array(
                                            'align'		=> true,
                                            'mode'      => false,
                                            'jsx'       => true
                )
            )
        );
        
    }
}
