<?php
/*
WP_REST_Server::READABLE = ‘GET’
WP_REST_Server::EDITABLE = ‘POST’
WP_REST_Server::DELETABLE = ‘DELETE’
WP_REST_Server::CREATE = ‘PUT’
*/


add_action('rest_api_init', function() {
	register_rest_route( 'ihag', 'action-rest',
		array(
		'methods' 				=> 'POST', 
		'callback'        		=> 'ihagCallback'
		)
	);
});
function ihagCallback(WP_REST_Request $request){
	if ( check_nonce() ) {
		$params = $request->get_params();

		$subject = __('Contact - Nouveau message depuis votre site web','ihag');
			
		$body = 'Civilité : '.sanitize_text_field(stripslashes($_POST['civility'])).'</br>';
		$body .= 'Prénom : '.sanitize_text_field(stripslashes($_POST['firstname'])).'</br>';
		$body .= 'Nom : '.sanitize_text_field(stripslashes($_POST['lastname'])).'</br>';
		$body .= 'email : '.sanitize_text_field(stripslashes($_POST['email'])).'</br>';
		$body .= 'Téléphone : '.sanitize_text_field(stripslashes($_POST['phone'])).'</br>';
		$body .= 'Organisation : '.sanitize_text_field(stripslashes($_POST['company'])).'</br>';
		$body .= 'Sujet : '.sanitize_text_field(stripslashes($_POST['subject'])).'</br>';
		$body .= 'Commentaire : '.sanitize_text_field(stripslashes($_POST['message'])).'</br>';


		//$body .= $upload_file_text;
		
		$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'ihag') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
		wp_mail( get_option( 'admin_email'), $subject, $body, $headers);


		return new WP_REST_Response( 'Hello World', 200 );
	}
	return new WP_REST_Response( 'BAD NONCE', 401 );
}


function check_nonce(){
	global $wp_rest_auth_cookie;
	/*
	 * Is cookie authentication being used? (If we get an auth
	 * error, but we're still logged in, another authentication
	 * must have been used.)
	 */
	if ( true !== $wp_rest_auth_cookie && is_user_logged_in() ) {
		return false;
	}
	// Is there a nonce?
	$nonce = null;
	if ( isset( $_REQUEST['_wp_rest_nonce'] ) ) {
		$nonce = $_REQUEST['_wp_rest_nonce'];
	} elseif ( isset( $_SERVER['HTTP_X_WP_NONCE'] ) ) {
		$nonce = $_SERVER['HTTP_X_WP_NONCE'];
	}
	if ( null === $nonce ) {
		// No nonce at all, so act as if it's an unauthenticated request.
		wp_set_current_user( 0 );
		return false;
	}
	// Check the nonce.
	return wp_verify_nonce( $nonce, 'wp_rest' );
}




add_action('rest_api_init', function() {
	register_rest_route( 'ihag', 'newsletter-rest',
		array(
		'methods' 				=> 'POST', 
		'callback'        		=> 'newsletterResponse'
		)
	);
});

function newsletterResponse(WP_REST_Request $request){
	if ( check_nonce() ) {
		$params = $request->get_params();

		if(addUserMailChimp (sanitize_email( $_POST['newletter_email'] ))){
			return new WP_REST_Response( '', 200 );
		}		
	}
	return new WP_REST_Response( 'Game lost', 401 );
}

function addUserMailChimp($email){
	// API to mailchimp ########################################################
	$list_id = 'c5e9213f44';
	$authToken = '4e5ebad3ce8f76e0c360ea11466ae4de-us14';
	// The data to send to the API

	$postData = array(
		"email_address" => $email, 
		"status" => "subscribed", 
		/*"merge_fields" => array(
		"FNAME"=> $_POST["name"],
		"PHONE"=> $_POST["phone"])*/
	);

	// Setup cURL
	$ch = curl_init('https://us14.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
	curl_setopt_array($ch, array(
		CURLOPT_POST => TRUE,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_HTTPHEADER => array(
			'Authorization: apikey '.$authToken,
			'Content-Type: application/json'
		),
		CURLOPT_POSTFIELDS => json_encode($postData)
	));
	// Send the request
	//	$response = curl_exec($ch);
	$result = curl_exec($ch);
	error_log($result);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
	error_log($httpCode);
	return ($httpCode == 200);
}