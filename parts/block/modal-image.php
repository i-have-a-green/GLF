<?php
/**
 * Block name: Modale d'image
 */

$image = get_field('image');
$size = '350-350'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    echo wp_get_attachment_image( $image, $size, false, array('class' => 'zoom-image', 'data-zoom' => wp_get_attachment_image( $image, 'full')));
}
?>

