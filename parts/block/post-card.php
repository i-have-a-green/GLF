<?php
/**
 * Template part for displaying archive posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ihag
 */
?>
<a href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>" class="link-card-blog">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="post-thumbnail">
            <?php
        $image = get_field('image', $post->ID);
        $size = '350-350'; // (thumbnail, medium, large, full or custom size)
        if( $image ) {

            echo wp_get_attachment_image( $image, $size);
        }
        ?>
        </div>

        <div class="content">
            <header class="entry-header">
                <?php
                the_title( '<h3 class="entry-title">', '</h3>' );
                ?>
            </header><!-- .entry-header -->
            <?php the_excerpt();?>
        </div>
    </article><!-- #post-<?php the_ID(); ?> -->
</a>