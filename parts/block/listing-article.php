<?php
/**
 * Block name: Listing article
 */
$term_blog_type = false;
if (@get_queried_object()->term_id !== NULL) {
    $term_blog_type = get_term_by( 'term_id', get_queried_object()->term_id, 'category_news' );
}


if (is_archive()) {
    $terms = get_terms( array( 'taxonomy' => 'category_news' ) );
    if ( $terms ) { 
        echo '<div class="post-tags-filters">';
        ?>
        <a href="<?php echo get_post_type_archive_link('news');?>" <?php echo (!$term_blog_type)?'class="active"':'';?>><?php _e( 'Tous','ihag' ); ?></a>
        <?php
        foreach ( $terms as $term_post_tag ) :
            $active = '';
            if($term_blog_type && $term_blog_type->term_id == $term_post_tag->term_id){
                $active = 'class="active"';
            }
            ?>
                <a href="<?php echo esc_url( get_term_link( $term_post_tag ) ); ?>" <?php echo $active;?>><?php echo esc_attr( $term_post_tag->name ); ?></a>
            <?php 
        endforeach;?>
        <?php 
        echo '</div>';
    }

}
?>


<div class="postContainer alignwide">
    <?php
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

        if ($term_blog_type) {
            
            $args = array(
                'paged'             => $paged,
                'post_type'         => 'news',
                'post_status'       => 'publish',
                'tax_query'         => array(
                    array(
                        'taxonomy' => 'category_news',
                        'field'    => 'slug',
                        'terms'    => $term_blog_type->slug,
                    )
                )
            );      
        }else{
            $args = array(
                'paged'             => $paged,
                'post_type'         => 'news',
                'post_status'       => 'publish'
            );      
        }
        if (! is_archive()) {
            $args['posts_per_page'] = 3 ;    
        }

        global $the_query;
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                get_template_part( 'parts/block/post-card' );
            }
        }

    if (! is_archive()) {
        ?>
        <p class="has-text-align-right is-style-link custom-position-blog-link"><a href="<?php echo get_post_type_archive_link('news');?>"><?php _e('Découvrir toutes nos actualités','ihag');?></a></p>
        <?php
    }else {
        ?>
        <?php
    }
    
    
    ?>
</div>
<div class="pagination alignwide">
    <div class="pagination__prev">
        <?php previous_posts_link( 'Publications précédentes' ); ?>
    </div>
    <div class="pagination__next">
        <?php next_posts_link( 'Publications suivantes' ); ?> 
    </div>
</div>