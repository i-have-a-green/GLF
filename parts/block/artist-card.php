<?php
/**
 * Template part for displaying archive posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ihag
 */
global $post;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="post-thumbnail">
        <?php the_post_thumbnail( '' ); ?>
    </div>
    <?php
    echo '<h3>'.get_field('artist-firstname',$post->ID).'</h3>';
    echo '<h3>'.get_field('artist-lastname',$post->ID).'</h3>';
    ?>
</article><!-- #post-<?php the_ID(); ?> -->