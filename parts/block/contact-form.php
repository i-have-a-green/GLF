<?php
/**
 * Block name: Contact form
 */
?>
<div class="wp-block-group alignwide">
    <form class="" name="contact-form" id="contact-form" action="#" method="POST"> 
    <div class="wp-block-columns is-style-default">
        <div class="wp-block-column" style="flex-basis:33.33%">
            <figure class="wp-block-image size-full is-style-caption-right">
                <?php
                    $attachmentID = get_field('contact-image');
                    echo wp_get_attachment_image($attachmentID, 'full');
                ?>
            </figure>
        </div>

        <div class="wp-block-column is-style-bg-no-logo-text-img has-color-yellow-background-color has-background" style="flex-basis:66.66%">
            <div class="wp-block-group">
                <h2><?php _e('Formulaire','ihag');?></h2>
                <p><?php _e('* Champs obligatoires','ihag');?></p>
                    <div class="form-group">
                        <label class="h3-like" for="civility"><?php _e('Civilité :','ihag');?></label>
                        <select name="civility" id="civility">
                            <option value="none" selected disabled hidden><?php _e('Choisir une civilité','ihag');?></option>
                            <option value="Madame"><?php _e('Madame','ihag');?></option>
                            <option value="Monsieur"><?php _e('Monsieur','ihag');?></option>
                            <option value="Autre"><?php _e('Autre','ihag');?></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="h3-like" for="lastname"><?php _e('Nom :*','ihag');?></label>
                        <input type="text" name="lastname" id="lastname" required>
                    </div>
                    <div class="form-group">
                        <label class="h3-like" for="firstname"><?php _e('Prénom :*','ihag');?></label>
                        <input type="text" name="firstname" id="firstname" required>
                    </div>
                    <div class="form-group">
                        <label class="h3-like" for="company"><?php _e('Organisme :','ihag');?></label>
                        <input type="text" name="company" id="company">
                    </div>
                    <div class="form-group">

                        <label class="h3-like" for="email"><?php _e('Email :*','ihag');?></label>
                        <input type="email" name="email" id="email" required>
                    </div>
                    <div class="form-group">
                        <label class="h3-like" for="phone"><?php _e('Téléphone :','ihag');?></label>
                        <input type="tel" name="phone" id="phone">
                    </div>
                    <div class="form-group">
                        <label class="h3-like" for="subject"><?php _e('Je souhaite... *','ihag');?></label>
                        <select name="subject" id="subject" class="button" required>
                            <option value="none" selected disabled hidden><?php _e('Choisir un sujet','ihag');?></option>
                            <option value="Devenir ambassadeur / ambassadrice"><?php _e('Devenir ambassadeur / ambassadrice','ihag');?></option>
                            <option value="Devenir partenaire"><?php _e('Devenir partenaire','ihag');?></option>
                            <option value="Être bénévole"><?php _e('Être bénévole','ihag');?></option>
                            <option value="Faire un don ou suivre un don"><?php _e('Faire un don ou suivre un don','ihag');?></option>
                            <option value="M’inscrire à un évènement GLF"><?php _e('M’inscrire à un évènement GLF','ihag');?></option>
                            <option value="Obtenir des informations concernant les activités GLF"><?php _e('Obtenir des informations concernant les activités de GLF','ihag');?></option>
                            <option value="Proposer une collaboration"><?php _e('Proposer une collaboration','ihag');?></option>
                            <option value="Rejoindre l’équipe"><?php _e('Rejoindre l’équipe','ihag');?></option>
                            <option value="Rejoindre le réseau des artistes"><?php _e('Rejoindre le réseau des artistes','ihag');?></option>
                            <option value="Autre"><?php _e('Autre','ihag');?></option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group textarea">
            <label class="h3-like" for="message"><?php _e('Message :','ihag');?></label>
            <textarea type="text" name="message" id="message"></textarea>
        </div>
        <div class="form-group rgpdgrp">
            <input type="checkbox" name="rgpd" id="rgpd" required>
            <label for="rgpd"><?php echo sprintf(__('Je reconnais avoir pris connaissance et accepte les <a href="%s">mentions légales</a>', 'ihag'), get_privacy_policy_url());?></label>
        </div>
        <input type="submit" id="submitContact" value="<?php _e("Envoyer", "ihag");?>">
        <p class="responseMessage" id="responseContact"></p>
    </form>     
</div>