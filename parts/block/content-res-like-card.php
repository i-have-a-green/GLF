<?php
/**
 * Block name: Cartes ressources like
 */

echo '<div class="alignwide ressourcesContainer">';
// Check rows exists.
if( have_rows('ressource-card-info') ):
    // Loop through rows.
    while( have_rows('ressource-card-info') ) : the_row();
        $attr = '';
        if (get_sub_field('link')) {
            $attr = 'target="'. get_sub_field('link')['target'] .'" href="'.get_sub_field('link')['url'] .'"';
        }elseif (get_sub_field('file')) {
            $attr = 'href="'.get_sub_field('file').'" download';
        }

        $bg = '';
        if(get_sub_field('bg_color')){
            $bg = 'has-background-color has-color-'.get_sub_field('bg_color').'-background-color';
        }
        
        ?>
        <a <?php echo $attr;?> class="ressource-card alignwide <?php echo $bg;?>">
            <figure>
                <?php echo wp_get_attachment_image( get_sub_field('image'), 'full'); ?>
            </figure>
            <div class="content">
                <h3><?php the_sub_field('title');?></h3>
                <p><?php the_sub_field('description');?></p>
                <?php if (get_sub_field('link_text')) {
                    ?>
                    <p class="is-style-link">
                        <?php the_sub_field('link_text');?>    
                    </p>
                    <?php
                }
                ?>
            </div>
        </a>
        <?php
    endwhile;
endif;
echo '</div>';
?>

