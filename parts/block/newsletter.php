<?php
/**
 * Block name: Newsletter
 */

?>

<form action="" id="newsletterForm">
    <input type="email" name="newletter_email" id="newsletter" placeholder="<?php _e('Renseignez votre email','ihag');?>" required>
    <div class="form-group">
        <input type="checkbox" name="rgpdNews" id="rgpdNews" required>
        <label for="rgpdNews"><?php echo sprintf(__('J\'ai pris connaissance et accepte les <a href="%s">mentions légales</a>', 'ihag'), get_privacy_policy_url());?></label>
    </div>
    <button type="submit" id="submitNews"><?php _e('S\'inscrire','ihag');?></button>
    <p class="responseMessage" id="responseNews" data-responseko=""></p>
</form>