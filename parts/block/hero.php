<?php
/**
 * Block name: Hero global
 */

if ((is_page()||is_single()) && has_post_thumbnail() ) {
    $attachmentID = get_post_thumbnail_id();
    $url = wp_get_attachment_image_url($attachmentID, 'full');
}else if (is_archive()) {
    $attachmentID = get_field('replace_hero_archive','option');
    $url = wp_get_attachment_image_url($attachmentID, 'full');
}else{
    $attachmentID = get_field('replace_hero','option');
    $url = wp_get_attachment_image_url($attachmentID, 'full');
}

?>
<div class="hero-container alignfull" style="--background: url('<?php echo $url?>');">
    <div class="caption"><?php echo wp_get_attachment_caption($attachmentID);?></div>
</div>