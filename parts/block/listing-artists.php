<?php
/**
 * Block name: Listing artistes
 */
?>


<div class="postContainer artistesContainer alignwide" id="artistsList">
    <?php
   
        $args = array(
            'post_type'         => 'artists',
            'post_status'       => 'publish',
            'posts_per_page'    => -1,
            'meta_key'			=> 'artist-lastname',
            'orderby'			=> 'meta_value',
            'order'				=> 'ASC'
        );      

        $iArt = 0;
        $htmlArt = '';
        global $the_query;
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                if ($iArt < 8) {
                    get_template_part( 'parts/block/artist-card' );
                }else{
                    
                    $htmlArt .= str_replace(array(chr(10),chr(13)),'',load_template_part( 'parts/block/artist-card' ));
                    
                }
                $iArt++;
            }
        }

        echo '<script> var htmlArt = \''. $htmlArt .'\'</script>';

    ?>
</div>
<button id="see-all-artists" onclick="artistLess()"><?php _e('Voir tous les artistes','ihag');?></button>