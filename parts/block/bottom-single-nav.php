
<?php
/**
 * Block name: bottom-section-nav
 */

?>

<div class="post-navigation-link ">
    <div class="post-navigation-link-previous  wp-block-post-navigation-link">
        <?php echo previous_post_link('%link');?>
    </div>
    <p class="has-text-align-center">
        <a href="<?php echo get_post_type_archive_link('news');?>">
            <?php _e('Retour aux actualités','ihag');?>
        </a>
    </p>
    <div class="post-navigation-link-next wp-block-post-navigation-link">
        <?php echo next_post_link('%link');?>
    </div>
</div>