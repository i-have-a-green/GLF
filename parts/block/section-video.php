
<?php
/**
 * Block name: section-video
 */

$embedHTML = get_field('video');
$image = get_field('picture');
?>
<?php if ($embedHTML && $image):?>
    <div id="dataContainer" class="modaleActivator" data-video="<?php echo htmlentities($embedHTML);?>">
        <img src="<?php echo $image;?>" alt="">
    </div>
<?php endif;?>